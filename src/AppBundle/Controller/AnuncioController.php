<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\AnuncioType;
use AppBundle\Entity\Anuncio;
use AppBundle\Entity\Categoria;
use AppBundle\Entity\Usuario;
use AppBundle\Entity\Oferta;
use AppBundle\Form\OfertaType;


/**
 * @Route("/anuncio")
 */
class AnuncioController extends Controller{
    /**
     * @Route("/ver", name="anuncio_ver")
     * @Template("AppBundle:Anuncio:verMisAnuncios.html.twig")
     */
    public function verAction()
    {
        $usuario = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $anuncios = $this->get('dwes.BLL.anuncio')->getAnunciosDeUsuario($usuario);

        return array(
            'anuncios' => $anuncios
        );
    }
    /**
     * @Route("/{id}/detalle", name="anuncio_verdetalle")
     * @Template("AppBundle:Anuncio:detalleAnuncio.html.twig")
     */
    public function verDetalleAction($id)
    {
        $anuncio = $this->get('dwes.BLL.anuncio')->getAnuncio($id);
        $ofertas = $this->get('dwes.BLL.oferta')->getOfertasDeAnuncio($anuncio);

        return array(
            'anuncio' => $anuncio,
            'ofertas' => $ofertas
        );

    }
    /**
     * @Route("/{id}/detalleyofertas", name="anuncio_vermidetalle")
     * @Template("AppBundle:Anuncio:detalleMiAnuncio.html.twig")
     */
    public function verMiDetalleAction($id)
    {
        $anuncio = $this->get('dwes.BLL.anuncio')->getAnuncio($id);
        $ofertas = $this->get('dwes.BLL.oferta')->getOfertasDeAnuncio($anuncio);

        return array(
            'anuncio' => $anuncio,
            'ofertas' => $ofertas
        );

    }
    private function getForm(Anuncio &$anuncio = null){
        $usuario = $this->get('security.token_storage')->getToken()->getUser();

        if ($anuncio === null){
            $anuncio = new Anuncio($usuario);
        }


        return $this->createForm(new AnuncioType(), $anuncio);
    }

    private function getDataInsertar($form){
        $em = $this->getDoctrine()->getManager();
        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/new", name="anuncio_new")
     * @Template("AppBundle:Anuncio:insertar.html.twig")
     * @Method({"GET"})
     */
    public function newAction(){
        $form = $this->getForm();

        return $this->getDataInsertar($form);
    }

    private function actualizaAnuncio(Request $request, $anuncio = null){
        if ($anuncio !== null)
            $edit = true;
        else
            $edit = false;

        $form = $this->getForm($anuncio);

        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            $em->persist($anuncio);
            $em->flush();

            return $this->redirect($this->generateUrl('anuncio_ver'));
        }

        if ($edit === true)
            return $this->getDataEdit($form, $anuncio);
        else
            return $this->getDataInsertar($form);
    }

    /**
     * @Route("/create", name="anuncio_create")
     * @Template("AppBundle:Anuncio:insertar.html.twig")
     * @Method({"POST"})
     */
    public function createAction(Request $request){
        return $this->actualizaAnuncio($request);
    }

    private function getDataEdit($form, $anuncio){
        return array(
            'anuncio' => $anuncio,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/{id}/edit", name="anuncio_edit")
     * @Template("AppBundle:Anuncio:insertar.html.twig")
     * @Method({"GET"})
     */
    public function editAction(Request $request, $id){
        $anuncio = $this->get('dwes.BLL.anuncio')->getAnuncio($id);
        $form = $this->getForm($anuncio);
        return $this->getDataEdit($form, $anuncio);
    }

    /**
     * @Route("/{id}/update", name="anuncio_update")
     * @Template("AppBundle:Anuncio:insertar.html.twig")
     * @Method({"POST"})
     */
    public function updateAction(Request $request, $id){
        $anuncio = $this->get('dwes.BLL.anuncio')->getAnuncio($id);
        return $this->actualizaAnuncio($request, $anuncio);
    }

    /**
     * @Route("/{id}/delete", name="anuncio_delete")
     * @Method({"GET"})
     */
    public function deleteAction($id)
    {
        $anuncio = $this->get('dwes.BLL.anuncio')->getAnuncio($id);

        $em = $this->getDoctrine()->getManager();

        $em->remove($anuncio);
        $em->flush();

        return $this->redirect($this->generateUrl('anuncio_ver'));
    }

}