<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Entity\Oferta;
use AppBundle\Form\OfertaType;
use AppBundle\Form\AnuncioType;
use AppBundle\Entity\Anuncio;
use AppBundle\Entity\Usuario;
use AppBundle\Form\UsuarioType;
use AppBundle\Entity\Seguimiento;
/**
 * @Route("/seguimiento")
 */
class SeguimientoController extends Controller
{
    /**
     * @Route("/{id}/new", name="seguir_new")
     * @Method({"GET"})
     */
    public function newAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuario = $this->get('security.token_storage')->getToken()->getUser();
        $anuncio = $this->get('dwes.BLL.anuncio')->getAnuncio($id);

        $seguimiento = $em->getRepository('AppBundle:Seguimiento')->findOneBy(array('usuario' => $usuario, 'anuncio' => $anuncio));
        if($seguimiento != null){
            return $this->redirect($this->generateUrl('seguimiento_listar'));
        }
        $seguimiento = new Seguimiento($usuario, $anuncio);


        $em->persist($seguimiento);
        $em->flush();

        return $this->redirect($this->generateUrl('seguimiento_listar'));


    }
    /**
     * @Route("/{id}/dejar", name="seguir_dejar")
     * @Method({"GET"})
     */
    public function dejarAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuario = $this->get('security.token_storage')->getToken()->getUser();
        $anuncio = $this->get('dwes.BLL.anuncio')->getAnuncio($id);

        $seguimiento = $em->getRepository('AppBundle:Seguimiento')->findOneBy(array('usuario' => $usuario, 'anuncio' => $anuncio));


        $em->remove($seguimiento);
        $em->flush();

        return $this->redirect($this->generateUrl('seguimiento_listar'));


    }
    /**
     * @Route("/listar", name="seguimiento_listar")
     * @Template("AppBundle:Seguimiento:verSeguidos.html.twig")
     */
    public function verAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuario = $this->get('security.token_storage')->getToken()->getUser()->getId();

        $seguidos = $em->getRepository('AppBundle:Seguimiento')->findBy(array('usuario' => $usuario));
        $anuncios = Array();
        $avisar = 'Ninguna novedad.';
        foreach($seguidos as $seguido){
            if($seguido->getAviso() == true){
                $seguido->setAviso(false);
                $em->persist($seguido);
                $avisar = 'Alguien ha hecho una oferta a un anuncio de tu lista.';
            }
            array_push($anuncios, $anuncio = $this->get('dwes.BLL.anuncio')->getAnuncio($seguido->getAnuncio()));

        }

        return array(
            'anuncios' => $anuncios,
            'avisar' => $avisar
        );
    }



}
