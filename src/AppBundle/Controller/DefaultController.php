<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Entity\Oferta;
use AppBundle\Form\OfertaType;
use AppBundle\Form\AnuncioType;
use AppBundle\Entity\Anuncio;
use AppBundle\Entity\Usuario;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="route_homepage")
     * @Template("AppBundle:default:index.html.twig")
     */
    public function indexAction(Request $request){
        $anuncios = $this->get('dwes.BLL.anuncio')->getAnuncios();
        return array(
            'mensaje' => 'Bienvenido al Club de la Quincalla',
            'fecha' => date('d-m-Y H:i'),
            'anuncios' => $anuncios
        );
    }
    /**
     * @Route("/profile/ofertas", name="oferta_ver")
     * @Template("AppBundle:Oferta:verMisOfertas.html.twig")
     */
    public function verAction()
    {
        $usuario = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $ofertas = $this->get('dwes.BLL.oferta')->getOfertasDeUsuario($usuario);
        return array(
            'ofertas' => $ofertas
        );
    }
}
