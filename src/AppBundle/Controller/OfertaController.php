<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Oferta;
use AppBundle\Form\OfertaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


/**
 * Oferta controller.
 *
 * @Route("/{idanuncio}/oferta")
 */
class OfertaController extends Controller
{


    /**
     * Lists all Oferta entities.
     *
     * @Route("/", name="oferta_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $ofertas = $em->getRepository('AppBundle:Oferta')->findAll();

        return $this->render('oferta/index.html.twig', array(
            'ofertas' => $ofertas,
        ));
    }

    /**
     * Creates a new Oferta entity.
     *
     * @Route("/new", name="oferta_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $idanuncio)
    {
        $usuario = $this->get('security.token_storage')->getToken()->getUser();
        $anuncio = $this->get('dwes.BLL.anuncio')->getAnuncio($idanuncio);
        $ofertas = $this->get('dwes.BLL.oferta')->getOfertasDeAnuncio($anuncio);
        $oferta = new Oferta($usuario, $anuncio);
        $form = $this->createForm('AppBundle\Form\OfertaType', $oferta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $anuncio->setNumOfertas($anuncio->getNumOfertas()+1);
            $em = $this->getDoctrine()->getManager();
            $seguidos = $em->getRepository('AppBundle:Seguimiento')->findBy(array('anuncio' => $anuncio));
         foreach($seguidos as $seguido){
             $seguido->setAviso(true);
             $em->persist($seguido);
         }

            $em->persist($anuncio);
            $em->persist($oferta);

            $em->flush();

            return $this->redirect($this->generateUrl('route_homepage'));
        }

        return $this->render('oferta/new.html.twig', array(
            'ofertum' => $oferta,
            'form' => $form->createView(),
            'anuncio' => $anuncio,
            'ofertas' => $ofertas
        ));
    }

    /**
     * Finds and displays a Oferta entity.
     *
     * @Route("/{id}", name="oferta_show")
     * @Method("GET")
     */
    public function showAction(Oferta $ofertum)
    {
        $deleteForm = $this->createDeleteForm($ofertum);

        return $this->render('oferta/show.html.twig', array(
            'ofertum' => $ofertum,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Oferta entity.
     *
     * @Route("/{id}/edit", name="oferta_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Oferta $ofertum)
    {
        $usuario = $this->get('security.token_storage')->getToken()->getUser();
        $deleteForm = $this->createDeleteForm($ofertum);
        $editForm = $this->createForm('AppBundle\Form\OfertaType', $ofertum);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ofertum);
            $em->flush();

            return $this->redirectToRoute('oferta_edit', array('id' => $ofertum->getId()));
        }

        return $this->render('oferta/edit.html.twig', array(
            'ofertum' => $ofertum,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/{idoferta}/delete", name="oferta_delete")
     * @Method({"GET"})
     */
    public function deleteOfertaAction($idanuncio, $idoferta)
    {
        $oferta = $this->get('dwes.BLL.oferta')->getOferta($idoferta);
        $anuncio = $this->get('dwes.BLL.anuncio')->getAnuncio($idanuncio);

        $anuncio->setNumOfertas($anuncio->getNumOfertas()-1);
        $em = $this->getDoctrine()->getManager();$em->persist($anuncio);
        $em->remove($oferta);
        $em->flush();

        return $this->redirect($this->generateUrl('route_homepage'));
    }


}
