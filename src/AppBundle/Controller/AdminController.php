<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Entity\Oferta;
use AppBundle\Form\OfertaType;
use AppBundle\Form\AnuncioType;
use AppBundle\Entity\Anuncio;
use AppBundle\Entity\Usuario;
use AppBundle\Form\UsuarioType;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="admin_users")
     * @Template("AppBundle:default:adminUsers.html.twig")
     */
    public function adminUsersAction(){
        $usuarios = $this->get('dwes.BLL.usuario')->getUsuarios();
        return array(
            'usuarios' => $usuarios
        );
    }

    private function getForm(Usuario &$usuario = null){

        if ($usuario === null){
            $usuario = new Usuario();
        }


        return $this->createForm(new UsuarioType(), $usuario);
    }

    private function getDataInsertar($form){
        $em = $this->getDoctrine()->getManager();
        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/new", name="admin_usuario_new")
     * @Template("AppBundle:default:adminUserInsertar.html.twig")
     * @Method({"GET"})
     */
    public function newAction(){
        $form = $this->getForm();

        return $this->getDataInsertar($form);
    }

    private function actualizaUsuario(Request $request, $usuario = null){
        if ($usuario !== null)
            $edit = true;
        else
            $edit = false;

        $form = $this->getForm($usuario);

        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            $em->persist($usuario);
            $em->flush();

            return $this->redirect($this->generateUrl('route_homepage'));
        }

        if ($edit === true)
            return $this->getDataEdit($form, $usuario);
        else
            return $this->getDataInsertar($form);
    }

    /**
     * @Route("/create", name="admin_user_create")
     * @Template("AppBundle:default:adminUserInsertar.html.twig")
     * @Method({"POST"})
     */
    public function createAction(Request $request){
        return $this->actualizaUsuario($request);
    }

    private function getDataEdit($form, $usuario){
        return array(
            'usuario' => $usuario,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/{id}/edit", name="admin_user_edit")
     * @Template("AppBundle:default:adminUserInsertar.html.twig")
     * @Method({"GET"})
     */
    public function editAction(Request $request, $id){
        $usuario = $this->get('dwes.BLL.usuario')->getUsuario($id);
        $form = $this->getForm($usuario);
        return $this->getDataEdit($form, $usuario);
    }

    /**
     * @Route("/{id}/update", name="admin_user_update")
     * @Template("AppBundle:default:adminUserInsertar.html.twig")
     * @Method({"POST"})
     */
    public function updateAction(Request $request, $id){
        $usuario = $this->get('dwes.BLL.usuario')->getUsuario($id);
        return $this->actualizaUsuario($request, $usuario);
    }

    /**
     * @Route("/{id}/delete", name="admin_user_delete")
     * @Method({"GET"})
     */
    public function deleteAction($id)
    {
        $usuario = $this->get('dwes.BLL.usuario')->getUsuario($id);
        $ofertas = $this->get('dwes.BLL.oferta')->getOfertasDeUsuario($id);
        $anuncios = $this->get('dwes.BLL.anuncio')->getAnunciosDeUsuario($id);
        $em = $this->getDoctrine()->getManager();
        foreach($ofertas as $oferta){
            $em->remove($oferta);
        }
        foreach($anuncios as $anuncio){
            $em->remove($anuncio);
        }
        $em->remove($usuario);
        $em->flush();

        return $this->redirect($this->generateUrl('route_homepage'));
    }

}
