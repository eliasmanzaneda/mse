<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\AnuncioType;
use AppBundle\Entity\Anuncio;
use AppBundle\Entity\Categoria;


/**
 * @Route("/categoria")
 */

class CategoriaController extends Controller{


    private function getForm(Anuncio &$anuncio = null)
    {
        if ($anuncio === null)
            $anuncio = new Anuncio();

        return $this->createForm(new AnuncioType(), $anuncio);
    }

    private function getDataInsertar($form)
    {
        $em = $this->getDoctrine()->getManager();

        $categoria = $em->getRepository('AppBundle:Categoria')->findAll();

        return array(
            'form' => $form->createView(),
            'categoria' => $categoria);
    }

}
