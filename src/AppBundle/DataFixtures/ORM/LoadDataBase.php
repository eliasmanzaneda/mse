<?
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Anuncio;
use AppBundle\Entity\Categoria;
use AppBundle\Entity\Oferta;
use AppBundle\Entity\Seguimiento;
use AppBundle\Entity\Usuario;

class LoadDataBase implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $userAdmin = new Usuario();
        $userAdmin->setUsername("Admin");
        $userAdmin->setEmail("admin@admin.admin");
        $userAdmin->setPassword('$2y$13$oycxqjuw3mokwkkg8g404uZ8rV/5x6dZ6Wrmwb5XWO1ydYbA57O9a');
        $userAdmin->setEnabled("true");
        $userAdmin->setRoles(array("ROLE_ADMIN"));

        $userAlex = new Usuario();
        $userAlex->setUsername("Alex");
        $userAlex->setEmail("alex@alex.alex");
        $userAlex->setPassword('$2y$13$rb7kaw6zois4soo8g0k44e38./LHwPGk2xRmqsmhpCfj2LszYemHa');
        $userAlex->setEnabled("true");
        $userAlex->setRoles(array("ROLE_USER"));

        $userPaco = new Usuario();
        $userPaco->setUsername("Paco");
        $userPaco->setEmail("paco@paco.paco");
        $userPaco->setPassword('$2y$13$nbfytqihpisc08044wok4urfCjzzbRTWS0KBDXHLhSI9NpV2ulxBS');
        $userPaco->setEnabled("true");
        $userPaco->setRoles(array("ROLE_USER"));

        $userElias = new Usuario();
        $userElias->setUsername("Elias");
        $userElias->setEmail("elias@elias.elias");
        $userElias->setPassword('$2y$13$o5w1cddwi1ccs00wkccc4OPcivBvzhQLp/WEOrHmwSYuJEoewgaOa');
        $userElias->setEnabled("true");
        $userElias->setRoles(array("ROLE_USER"));




        $manager->persist($userAdmin);
        $manager->persist($userAlex);
        $manager->persist($userPaco);
        $manager->persist($userElias);

        $categoriaMotor = new Categoria();
        $categoriaMotor->setNombre("Motor");

        $categoriaEducacion = new Categoria();
        $categoriaEducacion->setNombre("Educación");

        $categoriaElectronica = new Categoria();
        $categoriaElectronica->setNombre("Electrónica");

        $categoriaOtros = new Categoria();
        $categoriaOtros->setNombre("Otros");

        $manager->persist($categoriaMotor);
        $manager->persist($categoriaEducacion);
        $manager->persist($categoriaElectronica);
        $manager->persist($categoriaOtros);

        $anuncioCoche = new Anuncio($userPaco);
        $anuncioCoche->setCategoria($categoriaMotor);
        $anuncioCoche->setTitulo("Coche Seat Rojo");
        $anuncioCoche->setTexto("Seminuevo. Pocos Kilometros. Color Rojo.");
        $anuncioCoche->setImageUno("images/seatRojo.png");
        $anuncioCoche->setPrecio("500");

        $anuncioLibro = new Anuncio($userElias);
        $anuncioLibro->setCategoria($categoriaEducacion);
        $anuncioLibro->setTitulo("El Quijote de la Mancha");
        $anuncioLibro->setTexto("Muy bien conservado. Firma del autor original.");
        $anuncioLibro->setImageUno("images/libroQuijote.jpg");
        $anuncioLibro->setPrecio("30");

        $anuncioPortatil = new Anuncio($userAlex);
        $anuncioPortatil->setCategoria($categoriaElectronica);
        $anuncioPortatil->setTitulo("Portatil Alienware");
        $anuncioPortatil->setTexto("Poco ruido. Leds Verdes");
        $anuncioPortatil->setImageUno("images/alienware.png");
        $anuncioPortatil->setPrecio("200");
        $anuncioPortatil->setNumOfertas(2);

        $manager->persist($anuncioCoche);
        $manager->persist($anuncioLibro);
        $manager->persist($anuncioPortatil);

        $ofertaPotatilUno = new Oferta($userElias, $anuncioPortatil);
        $ofertaPotatilUno->setTitulo("Justo lo que necesitaba para clase");
        $ofertaPotatilUno->setTexto("Con esto ya no tendré problemas con mis maquinas virtuales.");
        $ofertaPotatilUno->setPrecio("170");

        $ofertaPotatilDos = new Oferta($userPaco, $anuncioPortatil);
        $ofertaPotatilDos->setTitulo("Perfecto para un colega");
        $ofertaPotatilDos->setTexto("Justo lo que necesitaba un colega mio.");
        $ofertaPotatilDos->setPrecio("120");

        $manager->persist($ofertaPotatilUno);
        $manager->persist($ofertaPotatilDos);

        $seguimientoPortatil = new Seguimiento($userElias, $anuncioPortatil);

        $manager->persist($seguimientoPortatil);

        $manager->flush();
    }
}