<?php
namespace AppBundle\BLL;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Validator\RecursiveValidator;

abstract class BaseBLL{
    protected $em;
    protected $validator;

    public function setEntityManager(EntityManager $em){
        $this->em = $em;
    }

    public function setValidator(RecursiveValidator $validator){
        $this->validator = $validator;
    }

    public function listar($nombreEntidad){
        $entities = $this->em->getRepository($nombreEntidad)->findAll();

        return array(
            'entities' => $entities,
        );
    }

    public function guarda($entity){
        $this->em->persist($entity);
        $this->em->flush();
    }
}