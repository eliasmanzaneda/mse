<?php

namespace AppBundle\BLL;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Entity\Categoria;

class CategoriaBLL extends BaseBLL
{
    public function getCategoria($id){
        $categoria = $this->em->getRepository('AppBundle:Categoria')->find($id);

        if($categoria === null)
            throw new NotFoundHttpException();

        return $categoria;
    }

    public function getCategorias(){
        return $this->em->getRepository('AppBundle:Categoria')->findAll();
    }

    public function nuevoCategoria($nombre){
        $categoria = new Categoria();
        $categoria->setNombre($nombre);


        $errors = $this->validator->validate($categoria);

        if (count($errors) > 0)
            return $errors;

        $this->guarda($categoria);

        return array();
    }

    public function editaCategoria($categoria, $nombre){
        $categoria->setTitulo($nombre);

        $errors = $this->validator->validate($categoria);

        if (count($errors) > 0)
            return $errors;

        $this->guarda($categoria);

        return array();
    }
}