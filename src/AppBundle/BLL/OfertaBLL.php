<?php

namespace AppBundle\BLL;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Entity\Oferta;


class OfertaBLL extends BaseBLL
{
    public function getOferta($id){
        $oferta = $this->em->getRepository('AppBundle:Oferta')->find($id);

        if($oferta === null)
            throw new NotFoundHttpException();

        return $oferta;
    }

    public function getOfertas(){
        return $this->em->getRepository('AppBundle:Oferta')->findAll();
    }
    public function getOfertasDeUsuario($usuario){
        return $this->em->getRepository('AppBundle:Oferta')->findBy(array('usuario' => $usuario));
    }
    public function getAnunciosConOfertasDeUsuario($usuario, $anuncio){
        return $this->em->getRepository('AppBundle:Oferta')->findBy(array('usuario' => $usuario), array('anuncio' => $anuncio));
    }
    public function getOfertasDeAnuncio($anuncio){
        return $this->em->getRepository('AppBundle:Oferta')->findBy(array('anuncio' => $anuncio));
    }
    public function nuevaOferta($titulo, $texto, $precio){
        $oferta = new Oferta();
        $oferta->setTitulo($titulo);
        $oferta->setTexto($texto);
        $oferta->setPrecio($precio);




        $errors = $this->validator->validate($oferta);

        if (count($errors) > 0)
            return $errors;

        $this->guarda($oferta);

        return array();
    }

    public function editaOferta($oferta, $titulo, $texto, $precio){
        $oferta->setTitulo($titulo);
        $oferta->setText($texto);
        $oferta->setPrecio($precio);


        $errors = $this->validator->validate($oferta);

        if (count($errors) > 0)
            return $errors;

        $this->guarda($oferta);

        return array();
    }
}