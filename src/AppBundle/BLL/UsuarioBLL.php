<?php

namespace AppBundle\BLL;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Entity\Usuario;


class UsuarioBLL extends BaseBLL
{
    public function getUsuario($id){
        $usuario = $this->em->getRepository('AppBundle:Usuario')->find($id);

        if($usuario === null)
            throw new NotFoundHttpException();

        return $usuario;
    }

    public function getUsuarios(){
        return $this->em->getRepository('AppBundle:Usuario')->findAll();
    }



    public function nuevoUsuario($nombre, $email, $roles){
        $usuario = new Usuario();
        $usuario->setNombre($nombre);
        $usuario->setEmail($email);
        $usuario->setRoles($roles);

        $errors = $this->validator->validate($usuario);

        if (count($errors) > 0)
            return $errors;

        $this->guarda($usuario);

        return array();
    }

    public function editaUsuario($usuario, $nombre, $email, $roles){
        $usuario->setNombre($nombre);
        $usuario->setEmail($email);
        $usuario->setRoles($roles);


        $errors = $this->validator->validate($usuario);

        if (count($errors) > 0)
            return $errors;

        $this->guarda($usuario);

        return array();
    }
}