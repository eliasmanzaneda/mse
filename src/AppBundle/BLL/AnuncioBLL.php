<?php

namespace AppBundle\BLL;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Entity\Anuncio;


class AnuncioBLL extends BaseBLL
{
    public function getAnuncio($id){
        $anuncio = $this->em->getRepository('AppBundle:Anuncio')->find($id);

        if($anuncio === null)
            throw new NotFoundHttpException();

        return $anuncio;
    }

    public function getAnuncios(){
        return $this->em->getRepository('AppBundle:Anuncio')->findAll();
    }
    public function getAnunciosDeUsuario($usuario){
        return $this->em->getRepository('AppBundle:Anuncio')->findBy(array('usuario' => $usuario));
    }
    public function getAnuncioDeOferta($oferta){
        return $this->em->getRepository('AppBundle:Anuncio')->findBy(array('id' => $oferta));
    }


    public function nuevoAnuncio($titulo, $texto, $precio, $categoria){
        $anuncio = new Anuncio();
        $anuncio->setTitulo($titulo);
        $anuncio->setTexto($texto);
        $anuncio->setCategoria($categoria);
        $anuncio->setPrecio($precio);




        $errors = $this->validator->validate($anuncio);

        if (count($errors) > 0)
            return $errors;

        $this->guarda($anuncio);

        return array();
    }

    public function editaAnuncio($anuncio, $titulo, $texto, $precio, $categoria){
        $anuncio->setTitulo($titulo);
        $anuncio->setText($texto);
        $anuncio->setCategoria($categoria);
        $anuncio->setPrecio($precio);


        $errors = $this->validator->validate($anuncio);

        if (count($errors) > 0)
            return $errors;

        $this->guarda($anuncio);

        return array();
    }
}