<?php
// src/AppBundle/Entity/Usuario.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class Usuario extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="user_image", type="string", length=255, nullable=true)
     */
    private $userimage;

    /**
     * @return string
     */
    public function getUserimage()
    {
        return $this->userimage;
    }

    /**
     * @param string $userimage
     */
    public function setUserimage($userimage)
    {
        $this->userimage = $userimage;
    }

    public function __construct()
    {
        parent::__construct();
        $this->userimage = "images/defaultUserImage.png";
        $this->setRoles(array('ROLE_USER'));

    }
    /**
     * @ORM\OneToMany(targetEntity="Anuncio", mappedBy="usuario")
     */
    private $anuncios;
    /**
     * @ORM\OneToMany(targetEntity="Oferta", mappedBy="usuario")
     */
    private $ofertas;
    /**
     * @ORM\OneToMany(targetEntity="Seguimiento", mappedBy="usuario")
     */
    private $seguidos;

    /**
     * Add anuncios
     *
     * @param \AppBundle\Entity\Anuncio $anuncios
     * @return Usuario
     */
    public function addAnuncio(\AppBundle\Entity\Anuncio $anuncios)
    {
        $this->anuncios[] = $anuncios;

        return $this;
    }

    /**
     * Remove anuncios
     *
     * @param \AppBundle\Entity\Anuncio $anuncios
     */
    public function removeAnuncio(\AppBundle\Entity\Anuncio $anuncios)
    {
        $this->anuncios->removeElement($anuncios);
    }

    /**
     * Get anuncios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnuncios()
    {
        return $this->anuncios;
    }

}
