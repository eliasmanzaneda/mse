<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Seguimiento
 *
 * @ORM\Table(name="seguimiento")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SeguimientoRepository")
 */
class Seguimiento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var boolean
     *
     * @ORM\Column(name="aviso", type="boolean")
     */
    private $aviso;



    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="seguidos")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", nullable=false)
     */
    private $usuario;

    public function __construct($usuario, $anuncio){
        $this->anuncio = $anuncio;;
        $this->usuario = $usuario;
        $this->aviso = false;
    }

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * @return mixed
     */
    public function getAnuncio()
    {
        return $this->anuncio;
    }
    /**
     * @return mixed
     */
    public function getAviso()
    {
        return $this->aviso;
    }


    /**
     * @param mixed $anuncio
     */
    public function setAnuncio($anuncio)
    {
        $this->anuncio = $anuncio;
    }

    /**
     * @ORM\ManyToOne(targetEntity="Anuncio", inversedBy="seguidos")
     * @ORM\JoinColumn(name="anuncio_id", referencedColumnName="id", nullable=false)
     */
    private $anuncio;


















    /**
     * @return boolean
     */
    public function isAviso()
    {
        return $this->aviso;
    }

    /**
     * @param boolean $aviso
     */
    public function setAviso($aviso)
    {
        $this->aviso = $aviso;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
