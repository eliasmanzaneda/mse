<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;
/**
 * Anuncio
 *
 * @ORM\Table(name="anuncio")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnuncioRepository")
 */
class Anuncio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="num_ofertas", type="integer")
     */
    private $numOfertas;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="texto", type="string", length=255)
     */
    private $texto;

    /**
     * @var string
     *
     * @ORM\Column(name="precio", type="string", length=255)
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="image_uno", type="string", length=255, nullable=true)
     */
    private $imageUno;

    /**
     * @var string
     *
     * @ORM\Column(name="image_dos", type="string", length=255, nullable=true)
     */
    private $imageDos;

    /**
     * @var string
     *
     * @ORM\Column(name="image_tres", type="string", length=255, nullable=true)
     */
    private $imageTres;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="string", length=255, nullable=true)
     */
    private $lat;



    /**
     * @var string
     *
     * @ORM\Column(name="lng", type="string", length=255, nullable=true)
     */
    private $lng;

    /**
     * @var string
     *
     * @ORM\Column(name="zoom", type="string", length=255, nullable=true)
     */
    private $zoom;

    /**
     * @var bool
     *
     * @ORM\Column(name="vendido", type="boolean")
     */
    private $vendido;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;


    /**
     * @ORM\ManyToOne(targetEntity="Categoria", inversedBy="anuncios")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id", nullable=false)
     */
    private $categoria;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="anuncios")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", nullable=false)
     */
    private $usuario;

    /**
     * @ORM\OneToMany(targetEntity="Oferta", mappedBy="anuncio")
     */
    private $ofertas;
    /**
     * @ORM\OneToMany(targetEntity="Seguimiento", mappedBy="anuncio")
     */
    private $seguidos;
    /**
     * Anuncio constructor.
     */
    public function __construct($usuario)
    {
        $this->imageUno = 'images/defaultAnuncioImage.png';
        $this->imageDos = 'images/defaultAnuncioImage.png';
        $this->imageTres = 'images/defaultAnuncioImage.png';
        $this->vendido = false;
        $this->lat = 60;
        $this->lng = -4;
        $this->zoom = 5;
        $this->numOfertas = 0;
        $this->fecha = new \DateTime();
        $this->usuario = $usuario;
    }
    /**
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param string $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return string
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param string $lng
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
    }

    /**
     * @return string
     */
    public function getZoom()
    {
        return $this->zoom;
    }

    /**
     * @param string $zoom
     */
    public function setZoom($zoom)
    {
        $this->zoom = $zoom;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return Anuncio
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set numOfertas
     *
     * @param integer $numOfertas
     * @return Anuncio
     */
    public function setNumOfertas($numOfertas)
    {
        $this->numOfertas = $numOfertas;

        return $this;
    }

    /**
     * Get numOfertas
     *
     * @return integer 
     */
    public function getNumOfertas()
    {
        return $this->numOfertas;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Anuncio
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set texto
     *
     * @param string $texto
     * @return Anuncio
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string 
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set precio
     *
     * @param string $precio
     * @return Anuncio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return string 
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set imageUno
     *
     * @param string $imageUno
     * @return Anuncio
     */
    public function setImageUno($imageUno)
    {
        $this->imageUno = $imageUno;

        return $this;
    }

    /**
     * Get imageUno
     *
     * @return string 
     */
    public function getImageUno()
    {
        return $this->imageUno;
    }

    /**
     * Set imageDos
     *
     * @param string $imageDos
     * @return Anuncio
     */
    public function setImageDos($imageDos)
    {
        $this->imageDos = $imageDos;

        return $this;
    }

    /**
     * Get imageDos
     *
     * @return string 
     */
    public function getImageDos()
    {
        return $this->imageDos;
    }

    /**
     * Set imageTres
     *
     * @param string $imageTres
     * @return Anuncio
     */
    public function setImageTres($imageTres)
    {
        $this->imageTres = $imageTres;

        return $this;
    }

    /**
     * Get imageTres
     *
     * @return string 
     */
    public function getImageTres()
    {
        return $this->imageTres;
    }


    /**
     * Set vendido
     *
     * @param boolean $vendido
     * @return Anuncio
     */
    public function setVendido($vendido)
    {
        $this->vendido = $vendido;

        return $this;
    }

    /**
     * Get vendido
     *
     * @return boolean 
     */
    public function getVendido()
    {
        return $this->vendido;
    }

    /**
     * Set categoria
     *
     * @param string $categoria
     * @return Anuncio
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return string 
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Anuncio
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }
}
