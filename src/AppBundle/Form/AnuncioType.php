<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class AnuncioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('texto')
            ->add('precio', IntegerType::class)
            ->add('categoria', EntityType::class, array(
                'class' => 'AppBundle:Categoria',
                'choice_label' => 'nombre'))
        ;

    }

    public function getName()
    {
        return 'anuncio';
    }

    public function configureOptions(OptionsResolver $options)
    {
        $options->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Anuncio',
        ));
    }
}
