<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class UsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('email')
        ;

    }

    public function getName()
    {
        return 'usuario';
    }

    public function configureOptions(OptionsResolver $options)
    {
        $options->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Usuario',
        ));
    }
}
